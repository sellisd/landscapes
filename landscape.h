#ifndef LANDSCAPE_H
#define LANDSCAPE_H
#include <iostream>
#include <math.h>
using namespace std;
class landscape{
  public:
    landscape(double A, double optX, double optY, double rho, double sigma, double P, double e){
      this->A = A;
      this->optimumX = optX;
      this->optimumY = optY;
      this->rho = rho;
      this->sigma = sigma;
      this->P = P;
      this->epsilon = e;
    };
    ~landscape(){};
    /*Super Gaussian function or higher order Gaussian (https://en.wikipedia.org/wiki/Gaussian_function#Two-dimensional_Gaussian_function)
    LaTeX
    $$
    W = A \cdot e^{(-\frac{(x-x_0)^2^P - \rho(x-x_0)(y-y_0) + (y-y_0)^2^P}{2\sigma^2})} + \epsilon
    $$
    ```{r
    x <- c(0:100)/25
    y <- c(0:100)/25
    w <- function(A, sigma, x0, y0, x, y, P, rho, epsilon){
      A*exp(-1*(((x-x0)^2^P - rho*(x-x0)*(y-y0) + (y-y0)^2^P)/(2*sigma^2)) ) + epsilon
    }
    #P integer
    m <- matrix(nrow=length(x), ncol = length(y))
    for(i in c(1:length(y))){
      m[, i] <- w(1,1,2,2,x,y[i],1, 1)
    }
    plot(x, w(1,1,2,2,x,y[50],2, 0, 0), type = "l", ylim = c(0,1))
    points(x, w(1,1,2,2,x,y[50],1, 0, 0), type = "l", lty = 3)
    contour(m)
    ```
    Important parameters
    A: Height multiplier
    e: Height ofset
    optX: optimum on X axis
    optY: optimum on Y axis
    rho: correlation, 0 no correlation
    sigma: shape
    P: power
    The maximum is at A-e
    */
    double f(double x, double y){
      double d = pow(2, P);
      double W = A*exp(-1.*((pow((x-optimumX),d) - rho*(x-optimumX)*(y-optimumY) + pow((y-optimumY), d))/(2*pow(sigma,2))) ) + epsilon;
      return(W);
    };
    void printMatrixF(double step){
      for(double x = 0; x < 4; x += step){
        for(double y = 0; y < 4; y += step){
          cout<<f(x,y)<<'\t';
        }
        cout<<endl;
      }
    }
  private:
    double A;
    double optimumX;
    double optimumY;
    double rho;
    double sigma;
    double P;
    double epsilon;
};
#endif
