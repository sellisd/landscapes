#include <iostream>
#include <cstdlib>
#include "landscape.h"

using namespace std;

int main(int argc, char* argv[]){
  double step = 0.1;
  landscape g = {1, 2, 2, 0, 1, 1, 0};
  //g.printMatrixF(step);
  /*smooth single speak
  A = 1
  optX = 2
  optY = 2
  rho = 0
  sigma = 1
  P = 1
  epsilon = 0
  */
  landscape v = {-1, 4, 4, 1.5, 1, 1, 1};
  //v.printMatrixF(step);
  /*valley
  A = -1
  optX = 4
  optY = 4
  rho = 1.5
  sigma = 1
  P = 1
  epsilon = 1
  */
  landscape r = {1, 0, 0, 1.5, 1, 1, 0};
  r.printMatrixF(step);
  /*ridge
  A = 1
  optX = 0
  optY = 0
  rho = 1.5
  sigma = 1
  P = 1
  epsilon = 0
  */

  return EXIT_SUCCESS;
}
